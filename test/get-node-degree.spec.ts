import "mocha";
import { expect } from "chai";

import Graph from "../lib";

describe("Graph.prototype.getNodeDegree", () => {
	const graph = new Graph({ A: null, B: null, C: null, D: null })
		.connect({ from: "A", to: "B" })
		.connect({ from: "A", to: "C" })
		.connect({ from: "C", to: "A" });

	it("returns number of connections, adjacent to a given node", () => {
		expect(graph.getNodeDegree("A")).to.equal(3);

		graph.connect({ from: "B", to: "C" });

		expect(graph.getNodeDegree("A")).to.equal(3);

		graph.connect({ from: "A", to: "D" });

		expect(graph.getNodeDegree("A")).to.equal(4);
	});

	it("counts loops twice", () => {
		expect(graph.getNodeDegree("A")).to.equal(4);

		graph.connect({ from: "A", to: "A" });

		expect(graph.getNodeDegree("A")).to.equal(6);
	});
});
