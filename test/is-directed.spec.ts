import "mocha";
import { expect } from "chai";

import Graph from "../lib";

describe("Graph.prototype.isDirected", () => {
	const directed = new Graph({ Rock: null, Paper: null, Scissors: null })
		.connect({ from: "Rock", to: "Scissors", dir: 1 })
		.connect({ from: "Scissors", to: "Paper", dir: 1 })
		.connect({ from: "Paper", to: "Rock", dir: 1 });

	const undirected = new Graph({ Kyiv: null, NewYork: null, Beijing: null })
		.connect({ from: "Kyiv", to: "NewYork", weight: 7507 })
		.connect({ from: "Kyiv", to: "Beijing", weight: 6447 })
		.connect({ from: "Beijing", to: "NewYork", weight: 10982 });

	it("indicates whether the graph is directed", () => {
		expect(directed.isDirected).to.be.true;
		expect(undirected.isDirected).to.be.false;
	});

	it("is properly cached", () => {
		expect(undirected.isDirected).to.be.false;

		undirected.connect({ from: "Kyiv", to: "NewYork", dir: 1 });

		expect(undirected.isDirected).to.be.true;
	});
});
