import "mocha";
import { expect } from "chai";

import Graph from "../lib";

describe("Graph.prototype.findEdges", () => {
	const graph = new Graph({ Atlanta: null, NewOrleans: null })
		.connect({ from: "Atlanta", to: "NewOrleans" });

	it("finds multiple edges by a given query", () => {
		expect(graph.findEdges({ to: "NewOrleans" })[0]).to.equal(graph.edges[0]);
	});

	it("returns empty array if no edges have been found", () => {
		expect(graph.findEdges({ weight: (w) => w === 42 }).length).to.equal(0);
	});
});
