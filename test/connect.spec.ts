import "mocha";
import { expect } from "chai";

import Graph from "../lib";

describe("Graph.prototype.connect", () => {
	const graph = new Graph({ A: null, B: null, C: null, D: null })
		.connect({ from: "A", to: "B" })
		.connect({ from: "B", to: "C" });

	it("adds new connection between nodes", () => {
		expect(graph.edges.length).to.equal(2);
		expect(() => graph.connect({ from: "A", to: "C" })).to.not.throw();
		expect(graph.edges.length).to.equal(3);
	});

	it("is chainable", () => {
		expect(graph.connect({ from: "C", to: "D" })).to.equal(graph);
	});
});
