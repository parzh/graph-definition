import { ThunkStrict } from "@valuer/types";
import { Cache } from "@parzh/cache";

/** Vertex of the graph */
export interface Node<Value = any> {
	/** Arbitrary payload, assigned to the node */
	value: Value;
}

/** Vertices' connection info */
export interface Edge<NodeID extends string = string> {
	/** Connection source */
	from: NodeID;

	/** Connection target */
	to: NodeID;

	/** Direction of the connection.  
	 * `1` represents direct direction (from `from` to `to`).  
	 * `-1` represents reverse direction (from `to` to `from`).  
	 * `0` represents edge with both directions possible.  
	 */
	dir?: 1 | -1 | 0;

	/** Weight of the connection */
	weight?: number;
}

// ***

/** @private */
type Query<Subject extends object> = {
	[Key in keyof Subject]?: Subject[Key] | ThunkStrict<Subject[Key]>;
};

/** @private */
const matchesQuery = <Subject extends object>(query: Query<Subject>): ThunkStrict<Subject> => (subject) => {
	for (const key in query) {
		const _key = key as keyof Query<Subject>;

		if (typeof query[_key] === "function")
			return (query[_key] as ThunkStrict)(subject[_key]);

		else return query[_key] === subject[_key];
	}

	return true;
};

/** @private */
const cache = new Cache<Graph<any, any>>();

// ***

/**
 * Collection of nodes and connections between them.
 *
 * @example
 * new Graph({ a: 42, b: 17 }, [
 * { from: "a", to: "b", weight: 5, dir: 1 },
 * { from: "b", to: "a", weight: 10, dir: 1 },
 * ]);
 *
 * new Graph({ rock: null, paper: null, scissors: null }, [
 * { from: "rock", to: "paper", dir: 1 },
 * { from: "paper", to: "scissors", dir: 1 },
 * { from: "rock", to: "scissors", dir: -1 },
 * ]);
 */
export default class Graph<ValuesMap extends Record<string, any>, NodeID extends keyof ValuesMap = keyof ValuesMap> {
	/** Map of nodes with encapsulated payloads */
	public readonly nodes = <{ [Key in NodeID]: Node<ValuesMap[Key]> }> Object.create(null);

	/** List of connections in the graph */
	public readonly edges: Edge<NodeID>[] = [];

	/**
	 * Whether the graph is directed.  
	 * The graph is considered directed if any of its connections is directed,
	 * i.e. `edge.dir` is either `1` or `-1`.
	 */
	get isDirected(): boolean {
		return cache.getValue(this, "isDirected", () => {
			return this.edges.some((edge) => edge.dir !== 0);
		});
	}

	// ***

	constructor(
		/** Map of payloads, each paired with a unique ID */
		valuesMap: ValuesMap,

		/** List of connections in the graph */
		edges: Edge<NodeID>[] = [],
	) {
		for (const key in valuesMap)
			this.nodes[key as NodeID] = { value: valuesMap[key] };

		for (const edge of edges)
			this.connect(edge);
	}

	/**
	 * Add new connection to the graph
	 * @param edge Connection between nodes
	 */
	connect(edge: Edge<NodeID>) {
		if (!(edge.from in this.nodes))
			throw new Error(`Source node "${ edge.from }" does not exist!`);

		if (!(edge.to in this.nodes))
			throw new Error(`Target node "${ edge.to }" does not exist!`);

		if (edge.weight == null)
			edge.weight = 1;

		if (edge.dir == null)
			edge.dir = 0;

		this.edges.push(edge);

		cache.invalidate(this, "isDirected");

		return this;
	}

	/**
	 * Find the first edge, that fits the query
	 * @uses `Array.prototype.find`
	 * @param query Map of requirements for an edge
	 */
	findEdge(query: Query<Edge<NodeID>>) {
		return this.edges.find(matchesQuery(query));
	}

	/**
	 * Find the first edge, that fit the query
	 * @uses `Array.prototype.filter`
	 * @param query Map of requirements for an edge
	 */
	findEdges(query: Query<Edge<NodeID>>) {
		return this.edges.filter(matchesQuery(query));
	}

	/**
	 * Find ID of the node, that fits the query
	 * @param query Map of requirements for an node
	 */
	findNodeID<Value extends ValuesMap[NodeID]>(query: Query<Node<Value>>) {
		const matches = matchesQuery(query);

		for (const nodeID in this.nodes)
			if (matches(this.nodes[nodeID]))
				return nodeID;
	}

	/**
	 * Find IDs of all the nodes, that fit the query
	 * @param query Map of requirements for an node
	 */
	findNodeIDs<Value extends ValuesMap[NodeID]>(query: Query<Node<Value>>) {
		const nodeIDs: NodeID[] = [];
		const matches = matchesQuery(query);

		for (const nodeID in this.nodes)
			if (matches(this.nodes[nodeID]))
				nodeIDs.push(nodeID);

		return nodeIDs;
	}

	/**
	 * Get degree (number of adjacent connections) of the node
	 * @param nodeID ID of the node to get degree of
	 */
	getNodeDegree(nodeID: NodeID): number {
		return [
			...this.findEdges({ from: nodeID }),
			...this.findEdges({ to: nodeID }),
		].length;
	}
}
