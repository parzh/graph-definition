import "mocha";
import { expect } from "chai";

import Graph from "../lib";

describe("Graph.prototype.findEdge", () => {
	const graph = new Graph({ Atlanta: null, NewOrleans: null })
		.connect({ from: "Atlanta", to: "NewOrleans" });

	it("finds a single edge by a given query", () => {
		expect(graph.findEdge({ from: "Atlanta" })).to.equal(graph.edges[0]);
	});

	it("returns `undefined` if no edges have been found", () => {
		expect(graph.findEdge({ weight: (w) => w === 42 })).to.be.undefined;
	});
});
