import "mocha";
import { expect } from "chai";

import Graph from "../lib";

describe("Graph.prototype.findNodeID", () => {
	const graph = new Graph({
		Kyiv: { population: 2.883e6 },
		NewYork: { population: 8.623e6 },
	});

	it("finds ID of the node by a given query", () => {
		expect(graph.findNodeID({ value: ({ population }) => population < 3e6 })).to.equal("Kyiv");
	});

	it("returns `undefined` if no nodes have been found", () => {
		expect(graph.findNodeID({ value: ({ population }) => population > 10e6 })).to.be.undefined;
	});
});
