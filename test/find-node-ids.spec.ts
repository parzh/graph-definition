import "mocha";
import { expect } from "chai";

import Graph from "../lib";

describe("Graph.prototype.findNodeIDs", () => {
	const graph = new Graph({
		Kyiv: { population: 2.883e6 },
		NewYork: { population: 8.623e6 },
	});

	it("finds multiple node IDs by a given query", () => {
		expect(graph.findNodeIDs({ value: ({ population }) => population > 2e6 })).to.deep.equal([ "Kyiv", "NewYork" ]);
	});

	it("returns empty array if no nodes have been found", () => {
		expect(graph.findNodeIDs({ value: ({ population }) => population > 10e6 }).length).to.equal(0);
	});
});
